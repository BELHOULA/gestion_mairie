from openerp.osv import osv, fields
from openerp import tools

class crm_claim_report(osv.Model):
  
    def init(self, cr):
 
        """ Display Number of cases And Section Name
        @param cr: the current row, from the database cursor,
         """
 
        tools.drop_view_if_exists(cr, 'crm_claim_report')
        cr.execute("""
            create or replace view crm_claim_report as (
              select
                    min(c.id) as id,
                    c.date as claim_date,
                    c.date_closed as date_closed,
                    c.date_deadline as date_deadline,
                    c.user_id,
                    c.stage_id,
                    a.code as code,
                    c.section_id,
                    c.partner_id,
                    c.company_id,
                    c.categ_id,
                    c.name as subject,
                    count(*) as nbr,
                    c.priority as priority,
                    c.type_action as type_action,
                    c.create_date as create_date,
                    avg(extract('epoch' from (c.date_closed-c.create_date)))/(3600*24) as  delay_close,
                    (SELECT count(id) FROM mail_message WHERE model='crm.claim' AND res_id=c.id) AS email,
                    extract('epoch' from (c.date_deadline - c.date_closed))/(3600*24) as  delay_expected
                from
                    crm_claim c
                    inner join crm_claim_stage a on c.stage_id=a.id
                group by c.date,\
                        c.user_id,c.section_id, c.stage_id,a.code,\
                        c.categ_id,c.partner_id,c.company_id,c.create_date,
                        c.priority,c.type_action,c.date_deadline,c.date_closed,c.id
            )""")

   
    _inherit = 'crm.claim.report'
    _columns = {
                'code' : fields.char('Code'),
               
              
              }    
    
    

    
