# encoding: utf8
from openerp.osv import fields, osv
from openerp import netsvc
from datetime import datetime

####################################################"
#############Rapport des dioutis##################
####################################################"
class report_abonnement(osv.osv_memory):
    _name = "report.xls.diouti"
    _description = "Export des dioutis"
    
    
    def abn_open_window(self, cr, uid, ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        period_obj = self.pool.get('account.period')
        fy_obj = self.pool.get('account.journal')
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, context=context)[0]
        result = mod_obj.get_object_reference(cr, uid, 'mairie_xls_export', 'action_account_voucher_diouti_tree')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        journal_id = data.get('periodicite', False) and data['periodicite'][0] or False
        result['context'] = str({'state':'open','journal_id': journal_id,'date_begin':data['date_begin'], 'date_end':data['date_end']})
        return result
    
    
    _columns = {
        'date_begin': fields.date('Date début', required=True),
        'date_end': fields.date('Date fin', required=True),
        'periodicite':fields.many2one('account.journal', 'Type impot', required=True),
                }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
