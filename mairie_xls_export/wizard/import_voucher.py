   # -*- coding: utf-8 -*-
#----------------------------------------------------------------------------
#
#    Copyright (C) 2015 .
#    Coded by: RCA
#
#----------------------------------------------------------------------------

import base64
from openerp.osv import osv, fields
from openerp import tools,netsvc
from tempfile import TemporaryFile
import tempfile
import csv
from openerp.tools.translate import _
import time
import datetime
try:
    import xlrd
except:
    raise Exception(_('Please install python lib xlrd: sudo apt-get install python-xlrd'))
        
class import_paiement(osv.osv):

    AVAILABLE_STATES = [
                      ('draft', 'Brouillon'),
                      ('failure', 'Echec'),
                       ('done', u'Traité'),
                     ]
    
    _name = 'import.import'
    _inherit = ['mail.thread']  
    _description = 'Import Tools' 
    _order = 'id desc' 
    
    _columns = {
        'name': fields.char("Date d'importation", size=128, readonly=True),
        'description': fields.text('Description de fichier'),
        'data': fields.binary('Fichier', required=True),
        'create_uid': fields.many2one('res.users', u'Importé par', readonly=True),
        'create_date': fields.date(u"Date d'importation", readonly=True),
        'state': fields.selection(AVAILABLE_STATES, 'Etat', select=True, readonly=True), 
        'montant': fields.float('Montant versé'),
        'journal_id': fields.many2one('account.journal', 'Type impot', required=True),
        
        }
    
    _defaults = {  
        'state' : 'draft',
        'name' : datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        }
   
    def import_paiements_verses(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        voucher_obj = self.pool.get('account.voucher')
        this = self.browse(cr, uid, ids[0])   
        wb_file = tempfile.NamedTemporaryFile()
        wb_file.write((base64.decodestring(this.data)))   
        # We ensure that cursor is at beginig of file
        wb_file.seek(0)
        workbook = xlrd.open_workbook(wb_file.name)
        montant=0
        if(workbook):
            for s in workbook.sheets():
                for row in range(s.nrows):
                    if(row>2):
                        data_row = []
                        for col in range(s.ncols):
                            if(col==0):
                                number = (s.cell(row, col).value)
                            elif(col==5):
                                amount = (s.cell(row, col).value)
                            elif(col==6):
                                verse = (s.cell(row, col).value)
                                if(verse=='Non'):
                                    bool=False
                                else:
                                    bool=True
                        id=voucher_obj.search(cr,uid,[('number' ,'=', number)])
                        val={
                           'number': number ,
                           'verse':bool,
                           'date_versement':datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                        }
                        voucher_obj.write(cr, uid,id, val,context=context)
                        if(bool==True and val['number']):
                            montant+=amount
            this.montant=montant
            this.state='done'
        else:
            this.state='failure'
       # print(montant) 
                    

 
