# encoding: utf8
from openerp import models, fields, api, _
from openerp.exceptions import except_orm, Warning, RedirectWarning
from openerp.osv import fields, osv
from openerp import netsvc
from datetime import datetime
from dateutil.relativedelta import relativedelta
import datetime
####################################################"
##################Rapport des abonnements##########################
####################################################"
class report_abonnement(osv.osv_memory):
    _name = "report.xls.abonnement"
    _description = "Export des abonnements"
    
    
    def abn_open_window(self, cr, uid, ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        period_obj = self.pool.get('account.period')
        fy_obj = self.pool.get('account.journal')
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, context=context)[0]
        result = mod_obj.get_object_reference(cr, uid, 'mairie_xls_export', 'action_account_invoice_tree')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        journal_id = data.get('periodicite', False) and data['periodicite'][0] or False
#         result['periods'] = []
        #if data['date_begin'] and data['date_end']:
             #date_begin = data.get('date_begin', False) and data['date_begin'][0] or False
             #date_end = data.get('date_end', False) and data['date_end'][0] or False
#             result['periods'] = period_obj.build_ctx_periods(cr, uid, period_from, period_to)
        if (data['date_begin']<=data['date_end']):
            result['context'] = str({'state':'open','journal_id': journal_id,'date_begin':data['date_begin'], 'date_end':data['date_end']})
        else:
            raise except_orm(_('Warning!'), _('La date de fin est inférieure à la date de début.'))
#            raise Warning(_('La date de fin est inférieure à la date de début.'))
           
#         if fiscalyear_id:
#             result['name'] += ':' + fy_obj.read(cr, uid, [fiscalyear_id], context=context)[0]['code']
        return result
    
    
    _columns = {
        'date_begin': fields.date('Date début', required=True),
        'date_end': fields.date('Date fin', required=True),
        #'abn_payes':fields.boolean("Les abonnements payés"),
        'periodicite':fields.many2one('account.journal', 'Type impot', required=True),
                }

    
        
        
#         
#         
#         
#         
#         if context is None:
#             context = {}
#         data = {}
#         
#         data['form'] = self.read(cr, uid, ids, ['periodicite', 'date_begin', 'date_end','abn_payes'], context=context)[0]
#         for field in ['periodicite', 'date_begin', 'date_end','abn_payes']:
#             if isinstance(data['form'][field], tuple):
#                 data['form'][field] = data['form'][field][0]
#       #  used_context = self._build_contexts(cr, uid, ids, data, context=context)
#         return self.pool['report'].get_action(cr, uid, [], 'gestion_mairie.raport_abonnement_paye', data=data, context=context)
# 
#     def _print_report(self, cr, uid, ids, data, context=None):
#         # data = self.pre_print_report(cr, uid, ids, data, context=context)
#        # context['landscape'] = True
#        if(data['form']['abn_payes']):
#            return self.pool['report'].get_action(cr, uid, [], 'gestion_mairie.raport_abonnement_paye', data=data, context=context)
#        else:
#            return self.pool['report'].get_action(cr, uid, [], 'gestion_mairie.raport_abonnement_impaye', data=data, context=context)
#    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
