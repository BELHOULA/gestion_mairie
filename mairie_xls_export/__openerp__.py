{
    "name": " Mairie XLS Export",
    "version": "1.0",
    "depends": ["base",'report_xls', 'account_voucher', 'product', 'account', 'gestion_mairie'],
    "author": "RCA",
    "category": "outil Général",
    "description": """
    Ce module permet la génération des rapports sous format xls ( les exportés et les importés), 
    d'ajouter la partie d'analyse ( des vues des totaux des paiements versés et des paiements efféctués).
    
    """,
    "init_xml": [],
    'data': [
             "wizard/wizard.xml",
             "reports/voucher_line_list_xls.xml",
             
                   ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
