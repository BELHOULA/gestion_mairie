# -*- encoding: utf-8 -*-

from openerp.osv import osv, fields
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import _


class account_voucher(osv.osv):
    _inherit = 'account.voucher'

    # override list in custom module to add/drop columns or change order
    def _report_xls_fields(self, cr, uid, context=None):
        return [
#             'move', 'name', 'date', 'journal', 'period', 'partner', 'account',
#             'date_maturity', 'debit', 'credit', 'balance',
#             'reconcile', 'reconcile_partial', 'analytic_account',


            #'ref', 'partner_ref', 'tax_code', 'tax_amount', 'amount_residual',
            #'amount_currency', 'currency_name', 'company_currency',
            #'amount_residual_currency',
            #'product', 'product_ref', 'product_uom', 'quantity',
            #'statement', 'invoice', 'narration', 'blocked',
            'number','date','reference','partner', 'name','amount' ,'verse'
        ]

    # Change/Add Template entries
    def _report_xls_template(self, cr, uid, context=None):
        """
        Template updates, e.g.

        my_change = {
            'move':{
                'header': [1, 20, 'text', _('My Move Title')],
                'lines': [1, 0, 'text', _render("line.move_id.name or ''")],
                'totals': [1, 0, 'text', None]},
        }
        return my_change
        """
        return {}
    
    _columns = {
                'verse': fields.boolean('Verse au perception'),
                'date_versement': fields.date("Date de versement")
                }
    _default = {
               #'verse': False,
                }
    
 