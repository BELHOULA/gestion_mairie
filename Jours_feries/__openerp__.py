{
    "name": "Jours Fériés",
    "version": "1.0",
    "depends": ["base"],
    "author": "RCA",
    "category": "outil Général",
    "description": """
    Ce module permet la saisie des jours fériés
    """,
    "init_xml": [],
    'data': [
                "jours_feries.xml",
                   ],
    'demo_xml': [],
    'installable': True,
    'active': False,
}
