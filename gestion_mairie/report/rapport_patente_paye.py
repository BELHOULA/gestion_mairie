from openerp.osv import osv, fields
from openerp import tools
from openerp.report import report_sxw
from openerp.addons.account.report.common_report_header import common_report_header
import convertion

class rrapport_patente_paye(report_sxw.rml_parse, common_report_header):
    _name = 'report.patente.paye'
    
    def get_start_date(self, data):
        if data.get('form', False) and data['form'].get('date_begin', False):
            return data['form']['date_begin']
        return ''

    def get_end_date(self, data):
        if data.get('form', False) and data['form'].get('date_end', False):
            return data['form']['date_end']
        return ''
    
  
    
    def __init__(self, cr, uid, name, context=None):
        super(rrapport_patente_paye, self).__init__(cr, uid, name, context=context) 
        self.montant=0.0       
        self.localcontext.update({
            'lines': self.lines,
            'get_start_date': self.get_start_date,
            'get_end_date': self.get_end_date,
            'get_somme': self.get_somme,
        })
        self.context = context
    
    def get_somme(self, data):
        requete="select cur.symbol as sym, cur.description as descrip from res_currency cur inner join res_company com on cur.id=com.currency_id "
        self.cr.execute(requete)
        res = self.cr.dictfetchall()
        currency=res[0]['descrip']+' '+res[0]['sym']
        montant_lettre=convertion.trad(self.montant,currency).upper()
        sommes={
                'montant': self.montant,
                'montant_lettre': montant_lettre,
               }
        return sommes      
        
    def lines(self, data):
        
        date_debut=data['date_begin']
        date_fin=data['date_end']
        requete="select av.date,\
             av.reference as num_quittance, \
             p.name as agent,\
             av.name as annee_paye,\
             av.amount \
        from account_voucher av \
            inner join res_partner p on p.id = av.partner_id \
            inner join account_journal aj on aj.id = av.journal_impot \
            where aj.code ='PT' and av.amount !=0 and av.state ='posted' and \
            av.date >= '"+date_debut+"' AND av.date <= '"+ date_fin +"' order by date"
            
        self.cr.execute(requete)
        res = []
        for date, num_quittance, agent,  annee_paye, amount in self.cr.fetchall():
            res.append({
                  'date':date,
                  'num_quittance':num_quittance,
                  'agent':agent,
                  'annee_paye':annee_paye,
                  'amount':amount,
                  
                  
                  })
            self.montant+=amount
         
        return res

    


class rapport_patente_paye(osv.AbstractModel):
    _name = 'report.gestion_mairie.rapport_patente_paye'
    _inherit = 'report.abstract_report'
    _template = 'gestion_mairie.rapport_patente_paye'
    _wrapped_report_class = rrapport_patente_paye    
