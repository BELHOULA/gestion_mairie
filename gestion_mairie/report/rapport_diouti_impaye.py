from openerp.osv import osv, fields
from openerp import tools
from openerp.report import report_sxw
from openerp.addons.account.report.common_report_header import common_report_header
import convertion

class rrapport_diouti_impaye(report_sxw.rml_parse, common_report_header):
    _name = 'report.diouti.impaye'
    
    def get_start_date(self, data):
        if data.get('form', False) and data['form'].get('date_begin', False):
            return data['form']['date_begin']
        return ''

    def get_end_date(self, data):
        if data.get('form', False) and data['form'].get('date_end', False):
            return data['form']['date_end']
        return ''
    
  
    
    def __init__(self, cr, uid, name, context=None):
        super(rrapport_diouti_impaye, self).__init__(cr, uid, name, context=context) 
        self.montant=0.0       
        self.localcontext.update({
            'lines': self.lines,
            'get_start_date': self.get_start_date,
            'get_end_date': self.get_end_date,
            'get_somme': self.get_somme,
        })
        self.context = context
    
    def get_somme(self, data):
        requete="select cur.symbol as sym, cur.description as descrip from res_currency cur inner join res_company com on cur.id=com.currency_id "
        self.cr.execute(requete)
        res = self.cr.dictfetchall()
        currency=res[0]['descrip']+' '+res[0]['sym']
        montant_lettre=convertion.trad(self.montant,currency).upper()
        sommes={
                'montant': self.montant,
                'montant_lettre': montant_lettre,
               }
        return sommes     
        
    def lines(self, data):
        
        date_debut=data['date_begin']
        date_fin=data['date_end']
        requete="select ai.date_invoice as date_facture,\
            ai.date_due as date_echeance,\
             ai.number as num_facture,  \
             p.name as agent,\
             p.matricule as matricule,\
             mz.name as zone, \
             ai.amount_total as amount\
        from account_invoice ai \
            inner join res_partner p on p.id = ai.partner_id \
            inner join account_journal aj on aj.id = ai.journal_id \
            inner join mairie_zone mz on mz.id = p.zone_id \
            where aj.code ='DT' and ai.state ='open' and \
            ai.date_due >= '"+date_debut+"' AND ai.date_due <= '"+ date_fin +"' order by date_due"
            
        self.cr.execute(requete)
        res = []
        for date_facture, date_echeance, num_facture,  agent, matricule, zone, amount in self.cr.fetchall():
            res.append({
                  'date_facture':date_facture,
                  'num_facture': num_facture,
                  'date_echeance':date_echeance,
                  'matricule': matricule,
                  'agent':agent,
                  'zone':zone,
                  'amount':amount,
                  })
            self.montant+=amount
         
        return res

    


class raport_diouti_impaye(osv.AbstractModel):
    _name = 'report.gestion_mairie.raport_diouti_impaye'
    _inherit = 'report.abstract_report'
    _template = 'gestion_mairie.raport_diouti_impaye'
    _wrapped_report_class = rrapport_diouti_impaye    
