# -*- coding: utf-8 -*-
import logging
import threading
import time
import psycopg2
from datetime import datetime
from dateutil.relativedelta import relativedelta

import openerp
from openerp import netsvc
from openerp.osv import fields, osv
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.safe_eval import safe_eval as eval
from openerp.tools.translate import _
from openerp.modules import load_information_from_description_file
# Modification Ajout de l'annee dans la type de periodicite dans le cron


_intervalTypes = {
    'work_days': lambda interval: relativedelta(days=interval),
    'days': lambda interval: relativedelta(days=interval),
    'hours': lambda interval: relativedelta(hours=interval),
    'weeks': lambda interval: relativedelta(days=7 * interval),
    'months': lambda interval: relativedelta(months=interval),
    'minutes': lambda interval: relativedelta(minutes=interval),
    
    # Ajout du type year dans l'interval pour permettre de caluculer la prochaine date dexecution 
    # lsq le type_dinterval est le years
    'years': lambda interval: relativedelta(years=interval),
    
}
# class ir_cron(osv.osv):
#     """ Model describing cron jobs (also called actions or tasks).
#     """
# 
#     # TODO: perhaps in the future we could consider a flag on ir.cron jobs
#     # that would cause database wake-up even if the database has not been
#     # loaded yet or was already unloaded (e.g. 'force_db_wakeup' or something)
#     # See also openerp.cron
#     
#      
#      
#     
# 
#     _inherit = "ir.cron"   
#     _columns = {        
#         'interval_type': fields.selection([('minutes', 'Minutes'),
#             ('hours', 'Hours'), ('work_days', 'Work Days'), ('days', 'Days'), ('weeks', 'Weeks'),
#             ('months', 'Months'), ('years', 'Annuelle'),
#             ], 'Interval Unit'),
#        
#     }
#     
#     
#     # reimplementer la methode 
#     def _process_job(self, job_cr, job, cron_cr):
#         """ Run a given job taking care of the repetition.
# 
#         :param job_cr: cursor to use to execute the job, safe to commit/rollback
#         :param job: job to be run (as a dictionary).
#         :param cron_cr: cursor holding lock on the cron job row, to use to update the next exec date,
#             must not be committed/rolled back!
#             print"je suis dedans "
#             
#         """
#         try:
#             now = datetime.now() 
#             nextcall = datetime.strptime(job['nextcall'], DEFAULT_SERVER_DATETIME_FORMAT)
#             numbercall = job['numbercall']
# 
#             ok = False
#             while nextcall < now and numbercall:
#                 if numbercall > 0:
#                     numbercall -= 1
#                 if not ok or job['doall']:
#                     self._callback(job_cr, job['user_id'], job['model'], job['function'], job['args'], job['id'])
#                 if numbercall:
#                     nextcall += _intervalTypes[job['interval_type']](job['interval_number'])
#                 ok = True
#             addsql = ''
#             if not numbercall:
#                 addsql = ', active=False'
#             cron_cr.execute("UPDATE ir_cron SET nextcall=%s, numbercall=%s" + addsql + " WHERE id=%s",
#                        (nextcall.strftime(DEFAULT_SERVER_DATETIME_FORMAT), numbercall, job['id']))
# 
#         finally:
#             job_cr.commit()
#             cron_cr.commit()
#     
# ir_cron()    


class mairie_scheduler(osv.osv_memory):
    
    _name = 'mairie.declaration'
    _columns = {
                'name' : fields.char('name', size=100,),
                'periodicite':fields.many2one('account.journal', 'Type impot', domain="[('type','=','sale')]", required=True),
                }   
    
    def schedule_appel_cotisations(self, cr, user, type, context):
        # liste des membres clients
        type_impot = self.pool.get("account.journal").search(cr, user, [('name', '=', type)]) 
        impot_ids = self.pool.get("gm.impot").search(cr, user, [('periodicite.name', '=', type)]) 
        ids = []
        clients_ids = []
         # boucle pour parcourir les clients
        for impot in self.pool.get("gm.impot").browse(cr, user, impot_ids, context):
            partner = impot.partner_id        
            clients_ids.append(partner.id)
            invoice_obj = self.pool.get('account.invoice')
             # recuperer le compte client
            account_id = partner.property_account_receivable and partner.property_account_receivable.id or False            
            payment_term = impot.periodicite.property_payment_term and impot.periodicite.property_payment_term.id or False
            if not  impot.existe_dec:            
                    invoice_id = invoice_obj.create(cr, user, {
                       'partner_id': partner.id,
                       'account_id': account_id,
                       'payment_term': payment_term,
                       'credit':partner.payment_amount_overdue,
                       'journal_id':impot.periodicite.id,
                       'fiscal_position': False
                           }, context) 
                                 
                    invoice_line_obj = self.pool.get('account.invoice.line')
                    amount = impot.patente                   
                    quantity = 1
                    line_value = {                           
                        'name':type
                        }
            # Recuperer les valeurs des champs calculer en fonction des produits, clients ...
                    line_dict = invoice_line_obj.product_id_change(cr, user, {},
                            False, False, quantity, '', 'out_invoice', partner.id, False, price_unit=amount, context=context)
                
                    line_value.update(line_dict['value'])
                    line_value['price_unit'] = amount
                  #  if impot.periodicite and impot.periodicite.taxe_id:
                  #      idx = impot.periodicite.taxe_id.id
                   #     line_value['invoice_line_tax_id'] = [(6, 0, [idx])]
                       
                    line_value['invoice_id'] = invoice_id
                    ids.append(invoice_id)
             # creation  de la ligne
                    invoice_line_id = invoice_line_obj.create(cr, user, line_value, context=context)
        
                    invoice_obj.write(cr, user, invoice_id, {'invoice_line': [(6, 0, [invoice_line_id])]}, context=context)
                    netsvc.LocalService('workflow').trg_validate(user, 'account.invoice', invoice_id, 'invoice_open', cr)
                  #  self.pool.get("gm.impot").write(cr, user, impot.id, {'existe_dec':True})
                        
        self.pool.get('res.partner').write(cr, user, clients_ids, {})
                  
    
    
    def schedule_appel_cotisation_mensuelle(self, cr, user, context={}):
       
        self.schedule_appel_cotisations(cr, user, 'Abonnement', context={})    
        return {} 
    
    
    def schedule_appel_cotisation_annuelle(self, cr, user, context={}):
       
        self.schedule_appel_cotisations(cr, user, 'Patente', context={})  
        self.schedule_appel_cotisations(cr, user, 'Taxe sur publicité', context={})  
        return {}  
    
    # Moi
    def schedule_appel_cotisation_hebdomadaire(self, cr, user, context={}):
       
        self.schedule_appel_cotisations(cr, user, 'Diouti', context={})    
        return {}  
    
    def appel_cotisation_mensuelle(self, cr, user, ids, context={}):
       
        self.schedule_appel_cotisations_abo(cr, user, context={})    
        return {} 
    
    
    def appel_cotisation_anuelle(self, cr, user, ids, context={}):
        obj = self.pool.get('account.analytic.account')
        wiz = self.browse(cr, user, ids[0], context)
        if wiz.periodicite.name == 'Diouti':
            self.appel_cotisation_diouti(cr, user, context={})
        elif wiz.periodicite.name == 'Patente':
            self.schedule_appel_cotisations(cr, user, wiz.periodicite.name, context)  
        else : 
            ids = obj.search(cr, user, [('state', '=', 'open'), ('amount_max', '!=', 0)])
            obj.recurring_create_invoice(cr, user, ids, context)    
        return {}  
    
    # Moi
    def appel_cotisation_diouti(self, cr, user, context={}):
        # verifier si le jour est ferie
        date = datetime.now()
        jours_feries = self.pool.get("jours.feries").search(cr, user, [('jour', '=', date.day),('mois','=', date.month )])
        if not jours_feries:
            # liste des membres clients
            type_impot = self.pool.get("account.journal").search(cr, user, [('code', '=', 'DT')]) 
            agent_ids = self.pool.get("res.partner").search(cr, user, [('agent', '=', True)]) 
            ids = []
            clients_ids = []
             # boucle pour parcourir les clients
            for partner in self.pool.get("res.partner").browse(cr, user, agent_ids, context):
                clients_ids.append(partner.id)
                invoice_obj = self.pool.get('account.invoice')
                 # recuperer le compte client
                account_id = partner.property_account_receivable and partner.property_account_receivable.id or False            
                payment_term = partner.property_payment_term and partner.property_payment_term.id or False
                invoice_id = invoice_obj.create(cr, user, {
                           'partner_id': partner.id,
                           'account_id': account_id,
                           'payment_term': payment_term,
                           'credit':partner.montant_diouti,
                           'journal_id':type_impot[0],
                           'fiscal_position': False
                               }, context) 
                                     
                invoice_line_obj = self.pool.get('account.invoice.line')        
                amount = partner.zone_id.diouti
                quantity = 1
                line_value = {                           
                            'name':'Diouti'
                            }
               
                # Recuperer les valeurs des champs calculer en fonction des produits, clients ...
                line_dict = invoice_line_obj.product_id_change(cr, user, {},
                                False, False, quantity, '', 'out_invoice', partner.id, False, price_unit=amount, context=context)
                    
                line_value.update(line_dict['value'])
                line_value['price_unit'] = amount
                      #  if impot.periodicite and impot.periodicite.taxe_id:
                      #      idx = impot.periodicite.taxe_id.id
                       #     line_value['invoice_line_tax_id'] = [(6, 0, [idx])]
                           
                line_value['invoice_id'] = invoice_id

                ids.append(invoice_id)
                 # creation  de la ligne
                invoice_line_id = invoice_line_obj.create(cr, user, line_value, context=context)
            
                invoice_obj.write(cr, user, invoice_id, {'invoice_line': [(6, 0, [invoice_line_id])]}, context=context)
                invoice_obj.action_date_assign(cr, user, [invoice_id], {})
                invoice_obj.action_move_create(cr, user, [invoice_id], {})
                invoice_obj.action_number(cr, user, [invoice_id], {})
                invoice_obj.invoice_validate(cr, user, [invoice_id], {})
                
                    #invoice_line_id = invoice_line_obj.create(cr, user, line_value, context=context)
        
                #invoice_obj.write(cr, user, invoice_id, {'invoice_line': [(6, 0, [invoice_line_id])]}, context=context)
                netsvc.LocalService('workflow').trg_validate(user, 'account.invoice', invoice_id, 'invoice_open', cr)
            self.pool.get('res.partner').write(cr, user, clients_ids, {})
                  
        return {}  
      
mairie_scheduler()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
