# encoding: utf8
from openerp.osv import fields, osv
from openerp import netsvc
from datetime import datetime

####################################################"
##################Rapport des abonnements##########################
####################################################"
class report_abonnement(osv.osv_memory):
    _name = "report.abonnement"
    _description = "Rapport des abonnements"
    
    _columns = {
        'date_begin': fields.date('Date début', required=True),
        'date_end': fields.date('Date fin', required=True),
        'abn_payes':fields.boolean("Les abonnements payés"),
        'periodicite':fields.many2one('account.journal', 'Type impot', domain="[('name','=','Abonnement')]", required=True),
                }
    
#     def _print_report(self, cr, uid, ids, data, context=None):
#         raise (_('Error!'), _('Not implemented.'))
#     
    def check_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = {}
        
        data['form'] = self.read(cr, uid, ids, ['periodicite', 'date_begin', 'date_end','abn_payes'], context=context)[0]
        for field in ['periodicite', 'date_begin', 'date_end','abn_payes']:
            if isinstance(data['form'][field], tuple):
                data['form'][field] = data['form'][field][0]
      #  used_context = self._build_contexts(cr, uid, ids, data, context=context)
        return self._print_report(cr, uid, ids, data, context=context)



    

    def _print_report(self, cr, uid, ids, data, context=None):
        # data = self.pre_print_report(cr, uid, ids, data, context=context)
       # context['landscape'] = True
       if(data['form']['abn_payes']):
           return self.pool['report'].get_action(cr, uid, [], 'gestion_mairie.raport_abonnement_paye', data=data, context=context)
       else:
           return self.pool['report'].get_action(cr, uid, [], 'gestion_mairie.raport_abonnement_impaye', data=data, context=context)
   
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
