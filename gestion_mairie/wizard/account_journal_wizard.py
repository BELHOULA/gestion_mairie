from openerp.osv import osv
from openerp.osv import fields, osv
from openerp import netsvc
from datetime import datetime


class account_journal_wizard(osv.osv_memory):

    _name = "account.journal.wizard"
    
    def create_account_journal(self, cr, user, ids, context={}):
        
        
        fields_to_default = ['is_impot_journal', 'code', 'loss_account_id', 'currency', 'internal_account_id',
 'account_control_ids', 'user_id', 'cash_control', 'centralisation', 'group_invoice_lines', 'company_id',
 'profit_account_id', 'type', 'default_debit_account_id', 'default_credit_account_id',
 'type_control_ids', 'sequence_id', 'allow_date', 'is_diouti', 'name', 'analytic_journal_id',
 'with_last_closing_balance', 'property_payment_term', 'entry_posted', 'cashbox_line_ids']
        
        wiz = self.browse(cr, user, ids[0], context)
        journal1 = wiz.journal1
        journal2 = wiz.journal2
        journal_obj = self.pool.get("account.journal")
        default_value = journal_obj.default_get(cr, user, fields_to_default)
       
  
        return {'type': 'ir.actions.act_window_close'}
    
    
#     def _get_default_account(journal_type, type='debit'):
#             # Get the default accounts
#             default_account = False
#             if journal_type in ('sale', 'sale_refund'):
#                 default_account = acc_template_ref.get(template.property_account_income_categ.id)
#             elif journal_type in ('purchase', 'purchase_refund'):
#                 default_account = acc_template_ref.get(template.property_account_expense_categ.id)
#             elif journal_type == 'situation':
#                 if type == 'debit':
#                     default_account = acc_template_ref.get(template.property_account_expense_opening.id)
#                 else:
#                     default_account = acc_template_ref.get(template.property_account_income_opening.id)
#             return default_account
#         
        
    _columns = {
        'journal1':fields.many2one('account.journal', 'Methode de paiement', domain="['|',('type','=','cash'),('type','=','bank')]", required=True),
        'journal2':fields.many2one('account.journal', "Type d'impot", domain="[('type','=','sale')]", required=True),
       # 'default_credit_account_id': _get_default_account(journal_type, 'credit'),
        #'default_debit_account_id': _get_default_account(journal_type, 'debit'),
                }

account_journal_wizard()
