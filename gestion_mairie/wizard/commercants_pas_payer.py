from openerp.osv import osv
from openerp.osv import fields, osv
from openerp import netsvc
from datetime import datetime


class commercants_payer(osv.osv_memory):

    _name = "commercants.payer"
    
    def print_commercants_non_pas_payer(self, cr, user, ids, context={}):
        
        
        partner_obj = self.pool.get("res.partner")
       
        wiz = self.browse(cr, user, ids[0], context)
        period = wiz.periodicite
        
        date_end = wiz.date_end
        date_end = datetime.strptime(date_end, '%Y-%m-%d')
        date_end = date_end.replace(hour=23, minute=59, second=59)
        
        date_begin = wiz.date_begin
        date_begin = datetime.strptime(date_begin, '%Y-%m-%d')
        date = datetime.now()
        if period.name == "Abonnement" :
            partner_ids = partner_obj.search(cr, user,
                                             [('payment_abonnement_earliest_due_date', '<=', date_end),
                                              ('payment_abonnement_earliest_due_date', '>=', date_begin)],
                                             context=context)
            print partner_ids
        
        elif period.name == "Diouti" :
            partner_ids = partner_obj.search(cr, user,
                                             [('payment_diouti_earliest_due_date', '<=', date_end),
                                              ('payment_diouti_earliest_due_date', '>=', date_begin), ],
                                             context=context)
            print partner_ids
        
        elif period.name == "Patente" :
            partner_ids = partner_obj.search(cr, user,
                                             [('payment_patente_earliest_due_date', '<=', date_end),
                                              ('payment_patente_earliest_due_date', '>=', date_begin), ],
                                             context=context)
            print partner_ids
                    

        return {'type': 'ir.actions.act_window_close'}
    
    def print_tout_non_pas_payer(self, cr, user, ids, context={}):
        
        
        partner_obj = self.pool.get("res.partner")
       
        wiz = self.browse(cr, user, ids[0], context)
        period = wiz.periodicite
       
       
        if period.name == "Abonnement" :
            partner_ids = partner_obj.search(cr, user,
                                             [('payment_abonnement_earliest_due_date', '<=', date_end),
                                              ('payment_abonnement_earliest_due_date', '>=', date_begin)],
                                             context=context)
            print partner_ids
        
        elif period.name == "Diouti" :
            partner_ids = partner_obj.search(cr, user,
                                             [('payment_diouti_earliest_due_date', '<=', date_end),
                                              ('payment_diouti_earliest_due_date', '>=', date_begin), ],
                                             context=context)
            print partner_ids
        
        elif period.name == "Patente" :
            partner_ids = partner_obj.search(cr, user,
                                             [('payment_patente_earliest_due_date', '<=', date_end),
                                              ('payment_patente_earliest_due_date', '>=', date_begin), ],
                                             context=context)
            print partner_ids
                    

        return {'type': 'ir.actions.act_window_close'}
    
    
    
    _columns = {
        'date_begin': fields.date('Date inferieur', required=True),
        'date_end': fields.date('Date superieur', required=True),
        'periodicite':fields.many2one('account.journal', 'Type impot', domain="[('type','=','sale')]", required=True),
                }

commercants_payer()
